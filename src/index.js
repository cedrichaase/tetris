const p5 = require('p5');
const Block = require('./objects/block');

class Tetris {
    constructor(settings) {
        this.settings = settings;
        this.snapToGrid();
    }

    snapToGrid() {
        const snap = dim => Math.floor((dim / this.settings.scale)) * this.settings.scale;

        this.settings.height = snap(this.settings.height) + 1;
        this.settings.width = snap(this.settings.width) + 1;
    }

    init() {
        const myp5 = new p5(p => {

            const createBlock = () => {
                return new Block(p, 0, 0, this.settings.scale, 1)
            };

            const blocks = [];
            blocks.unshift(createBlock());

            let block = blocks[0];

            addEventListener('keydown', event => {
                if (event.key === 'ArrowRight') {
                    if (block.rightMostPoint >= this.settings.width) {

                    }

                    block.x += this.settings.scale;
                }

                if (event.key === 'ArrowLeft') {
                    if (block.leftMostPoint <= 0) {
                        return;
                    }

                    block.x -= this.settings.scale;
                }

                if (event.key === 'ArrowUp') {
                    block.rotate();
                }

                if (event.key === 'ArrowDown') {
                    block.lower();
                }
            });

            p.setup = () => {
                p.createCanvas(this.settings.width, this.settings.height);
            };

            p.draw = () => {
                p.background(settings.background);
                if (block.lowestPoint >= this.settings.height) {
                    block.stop();
                    blocks.unshift(createBlock());
                    block = blocks[0];
                }

                blocks.forEach(blockToDraw => blockToDraw.draw());
            };
        }, this.settings.element);
    }
}


const settings = {
    element: '#tetris-container',
    height: window.innerHeight,
    width: 400,
    background: '#003264',
    scale: 20
};
const tetris = new Tetris(settings);
tetris.init();
