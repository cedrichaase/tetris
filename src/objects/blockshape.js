const Matrix = require('./matrix');

const shapes = [];

shapes.push([
    [0, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 0, 0]
]);

shapes.push([
    [1, 0, 0],
    [1, 0, 0],
    [1, 1, 0]
]);

shapes.push([
    [0, 0, 1],
    [0, 0, 1],
    [0, 1, 1]
]);

shapes.push([
    [0, 1, 1],
    [1, 1, 0],
    [0, 0, 0]
]);

shapes.push([
    [1, 1],
    [1, 1]
]);

module.exports = class BlockShape {
    static random() {
        const index = Math.floor(Math.random() * shapes.length);
        const shape = shapes[index];
        return new Matrix(shape.length, shape);
    }
};
