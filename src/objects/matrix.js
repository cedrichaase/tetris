function zeroArr(m, n) {
    return Array.apply(null, {length: m}).map(a => {
        return Array.apply(null, {length: n}).map(b => 0);
    });
}

function rotate(direction, matrix) {
    n = matrix.length;
    var r = zeroArr(n, n);
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++) {
            if (direction < 0) {
                r[i][j] = matrix[n - j - 1][i];
            } else {
                r[i][j] = matrix[j][n - i - 1];
            }
        }
    }
    return r;
}

function flatten(arr) {
    return [].concat(...arr)
}

module.exports = class Matrix {
    constructor(dim, matrix) {
        this.matrix = matrix || zeroArr(dim, dim);
        this.dim = dim;
    }

    rotate() {
        this.matrix = rotate(1, this.matrix);
    }

    get asArray() {
        return flatten(this.matrix);
    }

    get columns() {
        const cols = zeroArr(this.dim, this.dim);

        this.matrix.forEach((row, rowIndex) => {
            cols.forEach((col, colIndex) => {
                col[rowIndex] = row[colIndex];
            });
        });

        return cols;
    }
};
