const Matrix = require('./matrix');
const BlockShape = require('./blockshape');

module.exports = class Block {
    constructor(p, x, y, scale, speed) {
        this.p = p;
        this.x = x;
        this.y = y;
        this.scale = scale;
        this.speed = speed;

        this.ys = 0;

        this.shape = BlockShape.random();
    }

    draw() {
        this.p.rectMode(this.p.CENTER);
        this.p.stroke('#000');
        this.p.strokeWeight(1);
        this.p.fill('#fff');

        this.shape.asArray.forEach((val, i) => {
            const line = Math.floor(i / this.shape.dim);
            const column = i % this.shape.dim;

            if (val === 1) {
                this.p.rect(
                    this.x + column * this.scale + this.scale / 2,
                    this.y + line   * this.scale + this.scale / 2,
                    this.scale,
                    this.scale
                );
            }
        });

        this.ys += this.speed > 0 ? this.speed * 0.25 : 0;

        if (this.ys % this.scale === 0) {
            this.lower();
        }
    }

    rotate() {
        this.shape.rotate();
    }

    lower() {
        if (this.stopped) {
            return;
        }

        this.y += this.scale;
    }

    get lowestPoint() {
        for (const row of this.shape.matrix.slice().reverse()) {
            const rowIndex = this.shape.matrix.indexOf(row);

            const tileIndex = row.indexOf(1);

            if (tileIndex > -1) {
                return this.y + this.scale * (rowIndex + 2);
            }
        }

        return undefined;
    }

    get leftMostPoint() {
        const columns = this.shape.columns.slice();

        for (const col of columns) {
            const colIndex = columns.indexOf(col);

            const tileIndex = col.indexOf(1);

            if (tileIndex > -1) {
                return this.x + this.scale * colIndex;
            }
        }
    }

    get rightMostPoint() {
        const columns = this.shape.columns.slice();

        for (const col of columns.reverse()) {
            const colIndex = columns.indexOf(col);

            const tileIndex = col.indexOf(1);

            if (tileIndex > -1) {
                return this.x - this.scale * colIndex;
            }
        }
    }

    stop() {
        this.speed = 0;
        this.stopped = true;
    }
};
